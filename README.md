# SISTEMA DE GERENCIAMENTO DE LOCADORA

* É um sistema que será responsável pelos resgistros, inclusão e controle de filmes, funcionários de uma locadora de filmes que está sendo desenvolvido por alunos da Universidade Federal de São Paulo (UNIFESP), do curso de Tecnologia em Informática em Saúde, durante as disciplinas de 
Engenharia de Software e Programação Web. As funcionalidades planejadas são:


* Controle de locação: os funcionários poderão controlar entra de saida de filmes, bem como verificar atrasos e reservas de filmes.

* Controle de ponto: através do login, o administrador da locadora poderá controlar horários de entrada e saída dos funcionarios, bem como pausas para descanso.

* Controle financeiro: o administrador, bem como gerentes terão acesso aos registros financeiros e impressão de relatórios para o controle financeiro da locadora.

* Atulalização de catálogo: ambos os funcionários, assim como o administrador, poderão incluir novos titulos de filmes recém adquiridos, contudo somente gerente e administrador poderão alterar registros existentes.

* Lançamento de folha de ponto: administrador e gerente poderão imprir registro mensal de entrada e saída dos funcionários, servindo como folha de ponto.

* Lançamento documentos diversos: o administrador poderá gerar documentos diversos, tais como, advertências, tentativas de alterações no sistema, dentre outros.

* Notificação de atraso e reservas: o sistema acusará diariamente clientes que estão em atraso nas devoluções, bem como as reservas de tirulos do dia.

* Mobilidade: os clientes poderão acessar a plataforma para consulta e reservas de títulos através de smartphones e tablets.
